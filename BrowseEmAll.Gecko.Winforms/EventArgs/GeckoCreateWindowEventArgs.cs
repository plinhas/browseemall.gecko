﻿using BrowseEmAll.Gecko.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace BrowseEmAll.Gecko.Winforms
{
    public class GeckoCreateWindowEventArgs
        : CancelEventArgs
    {
        public readonly GeckoWindowFlags Flags;
        public readonly String Uri;

        public int InitialWidth = (int)nsIAppShellServiceConsts.SIZE_TO_CONTENT;
        public int InitialHeight = (int)nsIAppShellServiceConsts.SIZE_TO_CONTENT;

        /// <summary>Creates a new instance of a <see cref="GeckoCreateWindowEventArgs"/> object.</summary>
        /// <param name="flags"></param>
        /// <param name="uri"></param>
        public GeckoCreateWindowEventArgs(GeckoWindowFlags flags, String uri)
            : base(false)
        {
            Flags = flags;
            Uri = uri;
        }

        public GeckoWebBrowser WebBrowser { get; set; }

    }

}
