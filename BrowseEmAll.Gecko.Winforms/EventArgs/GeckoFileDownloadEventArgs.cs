﻿using BrowseEmAll.Gecko.Core;
using BrowseEmAll.Gecko.Core.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrowseEmAll.Gecko.Winforms
{
    public class GeckoFileDownloadEventArgs
        : EventArgs
    {
        public readonly Uri Uri;
        public readonly GeckoWindow DomWindow;
        public readonly Request Request;

        /// <summary>Creates a new instance of a <see cref="GeckoFileDownloadEventArgs"/> object.</summary>
        /// <param name="uri"></param>
        public GeckoFileDownloadEventArgs(Uri uri, GeckoWindow domWind, Request req)
        {
            Uri = uri;
            DomWindow = domWind;
            Request = req;
        }
    }
}
