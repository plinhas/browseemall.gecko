﻿using BrowseEmAll.Gecko.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrowseEmAll.Gecko.Winforms
{
    public class GeckoNavigatedEventArgs
        : EventArgs
    {
        // Wrapper is not often needed, so store only nsIRequest
        private nsIRequest _response;
        private GeckoResponse _wrapper;

        public readonly GeckoWindow DomWindow;

        public readonly Uri Uri;

        public readonly Boolean IsSameDocument;
        public readonly Boolean IsErrorPage;

        /// <summary>Creates a new instance of a <see cref="GeckoNavigatedEventArgs"/> object.</summary>
        /// <param name="value"></param>
        /// <param name="response"></param>
        internal GeckoNavigatedEventArgs(Uri value, nsIRequest response, GeckoWindow domWind, bool _sameDocument, bool _errorPage)
        {
            Uri = value;
            _response = response;
            DomWindow = domWind;

            IsSameDocument = _sameDocument;
            IsErrorPage = _errorPage;
        }

        public GeckoResponse Response
        {
            get { return _wrapper ?? (_wrapper = new GeckoResponse(_response)); }
        }
    }

}
