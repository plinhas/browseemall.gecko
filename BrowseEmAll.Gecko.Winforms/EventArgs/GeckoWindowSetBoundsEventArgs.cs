﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BrowseEmAll.Gecko.Winforms
{
    public class GeckoWindowSetBoundsEventArgs
        : EventArgs
    {
        public readonly Rectangle Bounds;
        public readonly BoundsSpecified BoundsSpecified;

        /// <summary>Creates a new instance of a <see cref="GeckoWindowSetBoundsEventArgs"/> object.</summary>
        /// <param name="bounds"></param>
        /// <param name="specified"></param>
        public GeckoWindowSetBoundsEventArgs(Rectangle bounds, BoundsSpecified specified)
        {
            Bounds = bounds;
            BoundsSpecified = specified;
        }
    }

}
