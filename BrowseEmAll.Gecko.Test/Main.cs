using System;
using System.Windows.Forms;
using BrowseEmAll;
using System.Runtime.InteropServices;
using System.Threading;
using System.IO;
using BrowseEmAll.Gecko.Core.DOM;
using BrowseEmAll.Gecko.Core.Events;
using BrowseEmAll.Gecko.Winforms;
using BrowseEmAll.Gecko.Core;

namespace BrowseEmAll.Gecko.Test
{
    class MainClass
    {
        [STAThread]
        public static void Main(string[] args)
        {
            // If you want to customize the GeckoFx PromptService then 
            // you will need make a class that implements some or all of nsIPrompt, 
            // nsIAuthPrompt2, and nsIAuthPrompt interfaces and
            // set the PromptFactory.PromptServiceCreator delegate. for example:
            // PromptFactory.PromptServiceCreator = () => new MyPromptService();
            // Gecko.PromptService already implements those interfaces, and may be sub-classed.
            

            string xulrunnerPath = XULRunnerLocator.GetXULRunnerLocation();

            Xpcom.Initialize(xulrunnerPath);
            // Uncomment the follow line to enable error page
            GeckoPreferences.User["browser.xul.error_pages.enabled"] = true;

            GeckoPreferences.User["gfx.font_rendering.graphite.enabled"] = true;

            GeckoPreferences.User["full-screen-api.enabled"] = true;

            Application.ApplicationExit += (sender, e) =>
            {
                Xpcom.Shutdown();
            };

            Form mainForm = new MyForm();

            Application.Run(mainForm);
        }

    }
}

