﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrowseEmAll.Gecko.Core.Interop;

namespace BrowseEmAll.Gecko.Core.Cache
{
	public static class ImageCache
	{
		private static ComPtr<imgICache> _imgCache;

		static ImageCache()
		{
			_imgCache = Xpcom.GetService2<imgICache>(Contracts.ImageCache);
		}

		public static void ClearCache(bool chrome)
		{
			_imgCache.Instance.ClearCache(chrome);
			
		}
	}
}
