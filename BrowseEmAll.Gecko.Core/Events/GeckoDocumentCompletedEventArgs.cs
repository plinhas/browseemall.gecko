﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrowseEmAll.Gecko.Core.Events
{
	public class GeckoDocumentCompletedEventArgs : EventArgs
	{
		public Uri Uri { get; private set; }
		public GeckoWindow Window { get; private set; }

		public GeckoDocumentCompletedEventArgs(Uri uri, GeckoWindow window)
		{
			this.Uri = uri;
			this.Window = window;
		}
	}
}