﻿using System;
using BrowseEmAll.Gecko.Core.Interop;

namespace BrowseEmAll.Gecko.Core
{
	public static class CertOverrideService
	{
		private static ComPtr<nsICertOverrideService> GetService()
		{
			return Xpcom.GetService2<nsICertOverrideService>(Contracts.CertOverride);
		}

		public static bool HasMatchingOverride(Uri url, nsIX509Cert cert)
		{
			if (url == null)
				throw new ArgumentNullException("url");

			using (var aHostName = new nsACString(url.Host))
			{
				uint flags = 0;
				bool isTemp = false;
				using (var overrideSvc = GetService())
				{
					return overrideSvc.Instance.HasMatchingOverride(aHostName, url.Port, cert, ref flags, ref isTemp);
				}
			}
		}

		/// <param name="flags">see nsICertOverrideServiceConsts</param>
		public static void RememberValidityOverride(Uri url, nsIX509Cert cert, int flags)
		{
			if (url == null)
				throw new ArgumentNullException("url");

			using (var aHostName = new nsACString(url.Host))
			{
				using (var svc = GetService())
				{
					svc.Instance.RememberValidityOverride(aHostName, url.Port, cert, (uint)flags, false);
				}
			}
		}
	}
}
